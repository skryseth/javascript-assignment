//All the declarations for buttons in the html
const btnBankPay = document.getElementById("btn-bank-pay");
const btnTransferToBank = document.getElementById("btn-transfer-to-bank");
const btnWork = document.getElementById("btn-work");
const btnApplyForLoan = document.getElementById("btn-apply-for-loan");
const btnBuyLaptop = document.getElementById("btn-buy-laptop");
const btnRepayLoan = document.getElementById("btn-pay-back-loan");
//All the declarations for the elements in the html
const pictureLaptopElement = document.getElementById("img-laptop");
const laptopPriceElement = document.getElementById("int-laptop-price");
const bankAmountElement = document.getElementById("int-bank-amount");
const workPayElement = document.getElementById("int-work-pay");
const laptopNameElement = document.getElementById("string-laptop-name");
const approvedForLoanElement = document.getElementById("string-loan-amount");
const laptopDescriptionElement = document.getElementById("string-laptop-description");
const remainingLoanAmountElement = document.getElementById("int-remaning-loan");
//All the declarations for the listed elements
const specsElement = document.getElementById("list-specs")
const laptopsElement = document.getElementById("options-laptops");

//Declaration of variables
let laptops = [];
let loanStatus = false;
let remainingLoanAmount = 0;
let imgUrl = "https://hickory-quilled-actress.glitch.me/"
let url = "https://hickory-quilled-actress.glitch.me/computers";


function setHiddenRepaybtn() {
    approvedForLoanElement.setAttribute("hidden","hidden");
    btnRepayLoan.setAttribute("hidden","hidden");
    btnApplyForLoan.removeAttribute("hidden");
}

function setHiddenLoanBtn() {
    approvedForLoanElement.removeAttribute("hidden");
    btnRepayLoan.removeAttribute("hidden");
    btnApplyForLoan.setAttribute("hidden","hidden");
}

//Fetching the json data from the API given in the assignment
fetch(url)
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops));
//Sets the options tag in the html and set the laptop in index 0 to default
const addLaptopsToMenu = (laptops) => {
    laptops.forEach( laptop => addLaptopToMenu(laptop));
    laptopPriceElement.innerText = `${laptops[0].price} Kr`;

    pictureLaptopElement.src =`${imgUrl}${laptops[0].image}`
    laptopNameElement.innerText = laptops[0].title;
    laptopDescriptionElement.innerText = laptops[0].description;
    //Gets the array within for the specs
    laptops[0].specs.forEach(spec => addSpecsToElement(spec));
}
//Creates and sets value to list elements in the html
const addSpecsToElement = (spec) => {
    const specElement = document.createElement("li");
    specElement.appendChild(document.createTextNode(spec));
    specsElement.appendChild(specElement)
}

//This sets one laptop to a specific option element 
const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option");
    //Sets the value so its easy to toggle between the options
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}
//This handle the laptop change in the options menu
const handleLaptopMenuChange = e => {
    //This selects the current laptop that is choosen in the options menu
    const selectedLaptop = laptops[e.target.selectedIndex];
    //Sets all the other elements in the html to be correct for the chosen laptop
    laptopPriceElement.innerText = `${selectedLaptop.price} Kr`;
    laptopNameElement.innerText = selectedLaptop.title;
    laptopDescriptionElement.innerText = selectedLaptop.description;
    //With computer 5 there is a 
    pictureLaptopElement.src = `${imgUrl}${selectedLaptop.image}`
    //A while loop to remove list elements
    while(specsElement.firstChild) {
        specsElement.removeChild(specsElement.firstChild);
    }
    //The new list elements for specs are being added to the html (created)
    selectedLaptop.specs.forEach(spec => addSpecsToElement(spec));
    
}
const handleBuyLaptop = () =>{
    //Chose the right computer to 
    const selectedLaptop = laptops[laptopsElement.selectedIndex];
    const selectedLaptopPrice = parseInt(selectedLaptop.price);
    const bankSubtract = parseInt(bankAmountElement.innerText) - selectedLaptopPrice;
    //Constraint if the person do not got enough money in the bank account
    if(bankSubtract < 0){
        alert("You do not got enough money for this computer");
    } else {
        //The user gets feedback that he/she is the proud new owner of selected computer
        alert(`Congrats with your new ${selectedLaptop.title}`);
        bankAmountElement.innerText = `${bankSubtract} Kr`;
    }
}
//handleWork btn
const handleWork = () => {
    //Increment the value by 100 for each click and update the value in field 
    const payAdd = parseInt(workPayElement.innerText) + 100;
    workPayElement.innerText = `${payAdd} Kr`;
}

//handleBank btn
const handlePayAddToBank = () => {
    const transferPay = parseInt(workPayElement.innerText);
    let bankedFunds = parseInt(bankAmountElement.innerText);
    //If the user has a loan then 10% of the users pay goes to downpayment on the loan
    if(loanStatus == true){
        let actualTransferdPay = transferPay - (transferPay * 0.1);
        remainingLoanAmount = remainingLoanAmount - (transferPay * 0.1);
        bankedFunds = actualTransferdPay + bankedFunds;
        remainingLoanAmountElement.innerText = remainingLoanAmount;
    } else {
        bankedFunds = transferPay + bankedFunds;
    }
    //Update bank amount and set workbalance to 0 
    bankAmountElement.innerText = `${bankedFunds} Kr`;
    workPayElement.innerText = 0; 
}


const applyForLoan = () => {

    const loanAmount = parseInt(prompt("Amount you wish to loan: "));
    const amountInBank = parseInt(bankAmountElement.innerText);
    //Checks if the input req is met and the loan constraint is met
    if((loanAmount > 0) && loanAmount <= (amountInBank * 0.5) && loanStatus == false){
        const newAmountInBank =  amountInBank + loanAmount;
        bankAmountElement.innerText = `${newAmountInBank} Kr`;
        //Updates loanstatus
        loanStatus = true;
        remainingLoanAmount = loanAmount;   
        //Here the button and the extra field is shown (repay loan btn and loan amount), and the button for 
        //applying for loan gets hidden.
        remainingLoanAmountElement.innerText = `${remainingLoanAmount} Kr`;
        setHiddenLoanBtn();
       
    } else {
        //Error msg to guide the user
        alert("You have entered a negative amount or the amount is larger than 50% of you bank balance")
    }
}

const repayLoan = () => {
    const repayAmount = parseInt(workPayElement.innerText)
    const updatedLoanAmount = remainingLoanAmount - repayAmount;
    const amountInBank = parseInt(bankAmountElement.innerText);
    //Checks if the amount is valid 
    if(updatedLoanAmount == 0){
        //When the loan is payed back to the bank, the user gets notified by a msg
        alert("Congratulations you have repayed your loan");
        loanStatus = false; 
        setHiddenRepaybtn();

    } else if(updatedLoanAmount < 0){
        const bankRepay = repayAmount - remainingLoanAmount;
        bankAmountElement.innerText = bankRepay + amountInBank;
        loanStatus = false;
        setHiddenRepaybtn();
    }
    remainingLoanAmount = updatedLoanAmount;
    remainingLoanAmountElement.innerText = remainingLoanAmount;
    workPayElement.innerText = `${0} Kr`

}
//Eventlistners 
laptopsElement.addEventListener("change", handleLaptopMenuChange);
btnRepayLoan.addEventListener("click", repayLoan);
btnWork.addEventListener("click", handleWork);
btnTransferToBank.addEventListener("click", handlePayAddToBank);
btnBuyLaptop.addEventListener("click",handleBuyLaptop);
btnApplyForLoan.addEventListener("click",applyForLoan);